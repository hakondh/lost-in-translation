import './App.css'
import { BrowserRouter as Router, Switch} from "react-router-dom"
import { useState } from 'react'
import PublicRoute from './hoc/PublicRoute'
import PrivateRoute from './hoc/PrivateRoute'
import { ROUTES } from "./constants/routes"
import Start from './components/Start/Start'
import Translate from './components/Translate/Translate'
import Profile from './components/Profile/Profile'
import Navbar from "./components/Navbar/Navbar"

function App() {
    /*  Common id state for children to use
        setId is passed to children that need to be able to update it */
    const [id, setId] = useState(localStorage.getItem('id'))

    return (
        <Router>
            <div className="App">
                <Navbar id={ id }/>
                <Switch>
                    <PublicRoute path={ ROUTES.Start } exact render={(props) => (
                        <Start { ...props } onIdChange={ setId } />
                    )} />
                    <PrivateRoute path={ ROUTES.Translate } component={ Translate }/>
                    <PrivateRoute path={ ROUTES.Profile } render={(props) => (
                        <Profile { ...props } onIdChange={ setId } />
                    )} />/>
                </Switch>
            </div>
        </Router>
    );
}

export default App;
