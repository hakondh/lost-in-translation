import { Redirect, Route } from 'react-router-dom'
import { ROUTES } from "../constants/routes";

export const PrivateRoute = props => {
    const id = localStorage.getItem('id')

    // If there is no id in localStorage, then redirect user to the start page
    if(!id) return <Redirect to={ ROUTES.Start } />

    return <Route {...props} />
}

export default PrivateRoute