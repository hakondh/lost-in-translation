function Signs({sentence}) {
    const letters = sentence.toLowerCase().split('')

    return (
        <div>
            {letters.map((letter, index) =>
            /* If it's a whitespace, then add some space. If it's a letter, show the letter */
            letter === ' '
            ? <span key={index}>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            : /* Images are stored in /public to make it possible to dynamically reference their paths. */
              <img key={index} src={`${process.env.PUBLIC_URL}/resources/individual_signs/${letter}.png`} alt={letter} />
            )}
        </div>
    )
}

export default Signs