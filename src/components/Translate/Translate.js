import Signs from './Signs'
import {useState} from "react";

function Translate() {
    const [text, setText] = useState('')
    const [translationText, setTranslationText] = useState('')

    const onInputChange = (e) => setText(e.target.value)

    const handleTranslation = () => {
        // Update localStorage
        let translations = JSON.parse(localStorage.getItem('translations'))
        if (translations.length < 10) {
            translations.unshift(text)
        } else {
            translations.pop()
            translations.unshift(text)
        }
        localStorage.setItem('translations', JSON.stringify(translations))

        //Remove anything that is not an English character or a whitespace
        const regex = /[^a-zA-Z\s]/g
        const cleanedText = text.replace(regex, '')

        // Set the text to translate
        setTranslationText(cleanedText)
    }

    return (
        <div className="container">
            <textarea  onChange={onInputChange} maxLength="40" placeholder="Enter text"/>
            <br/>
            <button className="btn" onClick={handleTranslation} type="button">Translate</button>
            {translationText.length > 0 && <Signs sentence={translationText}/>}
        </div>
    )
}

export default Translate