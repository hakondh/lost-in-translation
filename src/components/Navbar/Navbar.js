import logo from '../../assets/page-logo.png'
import { NavLink } from "react-router-dom";
import { ROUTES } from "../../constants/routes";
function Navbar({id}) {
    return (
        <nav>
            <ul className="nav-list">
                <li className="nav-item">
                    <NavLink to={ ROUTES.Start }>
                        <img src={logo} alt="logo" width="50em"/>
                    </NavLink>
                </li>
                <li className="nav-item">
                    <NavLink to={ ROUTES.Start } className="styled-link">
                        <h1 className="page-title">Lost in translation</h1>
                    </NavLink>
                </li>
                {id &&
                    <div className="nav-item-right">
                        <li className="nav-item" >
                            <NavLink className="styled-link" activeClassName="active"  to={ ROUTES.Translate }>Translate</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="styled-link" activeClassName="active"  to={ ROUTES.Profile }>{id}</NavLink>
                        </li>
                    </div>
                }
            </ul>
        </nav>
    )
}

export default Navbar