export function getUser(id) {
    return fetch(`http://localhost:5000/users/${id}`)
        .then(response => response.json())
}

export function postUser(id) {
    return fetch('http://localhost:5000/users', {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({id: id, translations: []})
    })
        .then(response => response.json())

}
