import { useState } from 'react'
import { getUser, postUser }  from './StartAPI'
import { useHistory } from "react-router-dom";
import { ROUTES } from "../../constants/routes";

function Start({ onIdChange }) {
    const history = useHistory()
    const [name, setName] = useState('')
    const [error, setError] = useState(false)

    const id = localStorage.getItem('id')

    const onInputChange = (e) => setName(e.target.value)

    const onGo = async () => {
        try {
            let user = await getUser(name)
            // If there is no user with this name, then post new user
            if(Object.keys(user).length === 0) user = await postUser(name)

            // Set user (new or not) in localStorage
            localStorage.setItem('id', user.id)
            // Set the users id in App (so all children can use it)
            onIdChange(user.id)
            // As localStorage only stores strings, we stringify the array so we easily can parse it when getting it
            localStorage.setItem('translations', JSON.stringify(user.translations))
            history.push(ROUTES.Translate)
        } catch(error) {
            setError(error.message)
        }
    }

    return (
        <div className="container">
            {id
                ? <div>
                    <h2>Enter a new name to switch profile!</h2>
                    <p>This will not save translations from this session. Log out to save.</p>
                  </div>
                : <h2>Enter your name to start translating!</h2>
            }
            <input className="inp" onChange={ onInputChange } placeholder="Your name..."/>
            <br/>
            {error && <p>{error}</p>}
            <button className="btn" onClick={ onGo }>Go!</button>
        </div>
    )
}

export default Start