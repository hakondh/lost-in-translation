import { useState} from 'react';
import { useHistory} from 'react-router-dom'
import { patchTranslations } from "./ProfileAPI";
import { ROUTES } from "../../constants/routes";

function Profile({onIdChange}) {
    const history = useHistory()
    const id = localStorage.getItem('id')
    const [translations, setTranslations] = useState(JSON.parse(localStorage.getItem('translations')))
    const [error, setError] = useState('')

    const clearTranslations = () => {
        localStorage.setItem('translations', '[]')
        setTranslations([])
    }

    const logOut = async () => {
        try {
            await patchTranslations(id, translations) // Update translations in db
            localStorage.removeItem('id')
            localStorage.removeItem('translations')
            onIdChange('')
            history.push(ROUTES.Start)
        }
        catch(error) {
            setError(error.message)
        }
    }

    return(
        <div className="container">
            <h1>{id}'s Profile</h1>
            <h3>Last 10 translations</h3>
            {translations.length !== 0
                ? translations.map((translation, index) => <p key={index}>{translation}</p>)
                : <p>No translations.</p>
            }
            <div className="btn-container">
                <button className="btn" onClick={ clearTranslations }>Clear translations</button>
                {' '}
                <button className="btn" onClick={ logOut }>Log out</button>
                {error && <p>{error}</p>}
            </div>
        </div>
    )
}

export default Profile