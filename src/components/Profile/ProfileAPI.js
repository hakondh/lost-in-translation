export function patchTranslations(id, translations) {
    return fetch(`http://localhost:5000/users/${id}`, {
        method: 'PATCH',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({translations: translations})
    })
        .then(response => response.json())
}