# Lost in translation

### by Håkon Dalen Hestnes and Michael Mellemseter

## About

A translation app for you to translate your sentences to sign language. The user has to make an account with an username to begin with, and then presented with a translation page. The user can also access it's own user page where the users last 10 translations is shown.
The application is made using react, and is also utilizing json-server and localStorage.

## How to run

- Clone the project
- Go to the root folder of the project
- Run `npm install`
- Run `npm run start`

- Run json-server: `json-server --watch db.json --port 5000`

Now the app is running at http://localhost:3000

## Code design/structure

The user gets access to translation by entering their name. If the name is already in db, the user will be fetched from there with any previous translations. If not, a new user is created. The user id and their translations are then stored in localStorage. Any translations the user now makes will be added to localStorage. When the user logs out the user with its translations is saved to db and then cleared from localStorage.

**1. assets** <br>
This folder contains static files. In this case, it contains the logo used in the navigation bar.

**2. components** <br>
This folder contains all the React components, as well as the API files that belong to them. They are stored in the subfolders *Navbar*, *Profile*, *Start* and *Translate*.

**3. constants** <br>
This folder contains any constants used throughout the project. In this case, routes. 

**4. css** <br>
This folder contains all the css, except for the css for the file index.js, which is stored in src. 

**5. hoc** <br>
This folder contains higher order components. In this case, this is PublicRoute and PrivateRoute, which is used to redirect the user if they try to access a private page without being "authenticated" (they have not entered their name).


